@extends('layouts.app')

@section('titulo','Contato')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Contato</h2>
            <p>Faça sua sugestão ou tire suas dúvidas sobre algum conteúdo publicado</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="#" method="POST">
                <div class="form-group">
                    <label for="nome">Nome:</label>
                    <input type="text" id="nome"  name="nome"
                    placeholder="Digite seu nome..." class="form-control">
                </div>
                <div class=form-group>
                    <label for="email">E-Mail:</label>
                    <input type="email" id="email" name="email"
                    placeholder="Digite seu E-mail Aqui..." class="form-control">
                </div>
                <div class="form-group">
                    <label for="tel">Telefone:</label>
                    <input type="text" id="tel" name="tel"
                    placeholder="(00) 0000-00000" class="form-control">
                </div>
                <div class="form-group">
                    <label for="assunto">Assunto:</label>
                    <input type="text" id="assunto" nome="assunto"
                    placeholder="Digite o Assunto" class="form-control">
                </div>
                <div class="form-group">
                    <label for="mensagem">Mensagem:</label>
                    <textarea id="mensagem" name="mensagem" placeholder="Digite sua Mensagem..." class="form-control"></textarea>
                    
                </div>
                <button type="submit"  class="btn btn-danger">
                    Enviar
                </button>
            </form>
        </div>
    </div>
</div>

@endsection
