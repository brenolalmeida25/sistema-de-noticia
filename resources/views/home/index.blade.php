@extends('layouts.app')

@section('titulo','home')

@section('conteudo')

<div class="container">
    <article class="row">
        <div class="col-md-4">
            <img class="img-fluid" src="https://via.placeholder.com/300x250">
        </div>
        <div class="col-md-8">
                <h2>Noticia Destaque</h2>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus, corporis at repudiandae necessitatibus earum provident nisi perspiciatis labore, culpa architecto illum ut dignissimos porro distinctio dolor tempora eveniet accusantium qui!</p>
            </div>
    </article>
    <div class="row">
        @for($i = 1; $i <=3; $i++)
        <div class="col-md-4 mt-5">
            <div class="card">
                <a href="#">
                    <img class="img-fluid" src="https://via.placeholder.com/500x250">
                </a>
                    <div class="card-body">
                        <h3 class="card-title"><a href="#">Titulo Notícia</a></h3>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt, cupiditate nihil sequi adipisci eum quasi, quisquam minus, debitis quo tenetur saepe earum explicabo laborum vitae veritatis libero id. Aliquid, repellat?</p>
                    </div> 
                    <div class="card-footer mt-5">
                        30/04/2019
                    </div>          
            </div>
        </div>
        @endfor
    </div>
</div>

@endsection