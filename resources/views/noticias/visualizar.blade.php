@extends('layouts.app')
@section('titulo','Tecnologia')

@section('conteudo')


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Tecnologia</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 mx-auto">
                <article class="box-noticia">
                    <h2>Título Notícia</h2>

                    <p>06/05/19</p>
                    <p class="text-center p-5">
                        <img class="img-fluid" src="https://via.placeholder.com/800x400">
                    </p>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque, ipsam commodi sed distinctio voluptatum assumenda incidunt doloribus reiciendis. Autem atque tenetur temporibus praesentium cupiditate ullam. Atque sunt asperiores et vitae! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatem deserunt magni illum vitae necessitatibus? Voluptate eveniet dolores nisi quisquam doloribus eligendi ea, sint accusantium tenetur saepe architecto natus quas odio.</p>
                </article>
            </div>
        </div>
    </div>

    <!---Vue.js & react.js--!>
@endsection